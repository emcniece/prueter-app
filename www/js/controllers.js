angular.module('starter.controllers', ['chart.js'])

.controller('AppCtrl', function($scope, $ionicModal, $timeout, $ionicSideMenuDelegate, $ionicLoading) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.data = {
    income: 45000,
    networth:100000
  };

  $scope.chartLabels = ["Speculation", "Growth and Diversification", "Savings and Accumulation", "Protection"];
  $scope.extraLabels = [
    "Art", "Futures", "Commodities", "Real Estate", "Tax Shelters", "Business Interests",
    "Bonds", "Equity Securities", "Mortgage Paydown",
    "TFSAs", "RESPs", "RRSPs", "Home Ownership", "Fixed Income Plans", "Mutual and Segregated Funds",
    "Will, POA, Trusts", "Debt Reduction", "Regular Savings", "Emergency Fund", "Disability Insurance", "Critical Illness Insurance", "Life Insurance"
  ]
  $scope.donutData = [];
  $scope.barData = [];

  $scope.updateChartData = function(){
    $scope.donutData = [
      $scope.data.income,
      $scope.data.networth,
      ($scope.data.income*.66),
      ($scope.data.networth*.66)
    ];

    if( $scope.chartLabels.length > 4){
      for(var i=4;i<$scope.chartLabels.length;i++){
        $scope.donutData.push( getRandomArbitrary(23000,1000000));
      }
    }
    console.log($scope.donutData);
    // Needs to be array because of series handling
    $scope.barData = [$scope.donutData];
  };

  $scope.updateChartData();

  // Menu opener
  $scope.openMenu = function(){
    $ionicSideMenuDelegate.toggleLeft();
  }
  $scope.closeMenu = function(){
    $ionicSideMenuDelegate.toggleLeft(false);
  }

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $ionicLoading.show({
      template: 'Sending...'
    });
    $timeout(function() {
      $ionicLoading.hide();
      $scope.closeLogin();
      $scope.closeMenu();
      $scope.signupComplete();
    }, 1000);
  };

  $scope.signupComplete = function(){
    angular.extend($scope.chartLabels, $scope.chartLabels, $scope.extraLabels);
    $scope.updateChartData();
    console.log( $scope.chartLabels.length);
  };

})

.controller('AboutCtrl', function($scope) {
  
})

.controller('PlaylistCtrl', function($scope, $stateParams) {
});
